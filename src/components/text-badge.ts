import { LitElement, css, html } from 'lit';
import { customElement } from 'lit/decorators.js';

@customElement('text-badge')
export class TextBadge extends LitElement {
  static get styles() {
    return css`
      :host {
        display: inline-block;
      }

      .text_badge {
        border-radius: 4px;
        background: black;
        color: white;
        padding: 2px 4px;
        margin-bottom: 2px;
      }
    `;
  }

  render() {
    return html`<div class="text_badge">
      <slot class="text_badge__content"></slot>
    </div>`;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'text-badge': TextBadge;
  }
}
