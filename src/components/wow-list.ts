import { LitElement, css, html } from 'lit';
import { customElement } from 'lit/decorators.js';

@customElement('wow-list')
export class WowList extends LitElement {
  static get styles() {
    return css`
      :host {
        --bullet-color: var(--pop-dark-gray, black);
      }

      ul {
        padding-left: 2rem;
        list-style: wowmoji;
      }

      li {
        line-height: 2;
        padding-inline-start: 0.5rem;
      }

      li::marker {
        font-size: 1.5rem;
      }

      @counter-style wowmoji {
        system: cyclic;
        symbols: "📷" "🕵️" "🌱";
        suffix: " ";
      }
    `;
  }

  render() {
    return html`
      <div>
        <slot name="north-star"></slot>
        <ul>
          <li><slot name="wow-1"></slot></li>
          <li><slot name="wow-2"></slot></li>
          <li><slot name="wow-3"></slot></li>
        </ul>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'wow-list': WowList;
  }
}
