import { LitElement, css, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';

@customElement('hello-hero')
export class HelloHero extends LitElement {
  static get styles() {
    return css`
      :host {
        --primary-color: var(--pop-dark-gray, black);
        --secondary-color: var(--pop-dark-gray, black);
      }

      figure {
        margin: 0;
        padding: 0;
      }

      figure > p {
        color: var(--primary-color);
        margin-top: 32px;
        margin-bottom: 0;
        padding-bottom: 0;
        line-height: 1.5rem;
        font-size: 1.5rem;
        font-weight: 700;
      }
    `;
  }

  @property({ type: Boolean }) hasVisited: boolean = false;

  private _saveState() {
    localStorage.setItem('hasVisited', 'true');
  }

  private _loadState(): boolean {
    const str = localStorage.getItem('hasVisited');
    if (str === null) {
      return false;
    }
    return str.toLowerCase() === 'true';
  }

  render() {
    if (this._loadState()) {
      this.hasVisited = true;
    } else {
      this._saveState();
    }
    return html`
      <div class="hero">
        <div class="hero__figure">
          <figure>
            <p>Hello, ${this.hasVisited ? 'again' : ''} 👋</p>
            <slot name="heading"></slot>
          </figure>
        </div>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'hello-hero': HelloHero;
  }
}
