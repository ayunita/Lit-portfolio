import { LitElement, css, html } from 'lit';
import { customElement } from 'lit/decorators.js';

@customElement('sticky-nav')
export class StickyNav extends LitElement {
  static get styles() {
    return css`
      :host {
      }

      nav {
        width: 100%;
        background: white;
        padding: 8px 0;
        top: 0;
        left: 0;
        text-align: center;
        position: fixed;
        z-index: 1;
        box-shadow:
          rgba(233, 66, 311, 0.4) 0px 5px,
          rgba(233, 66, 311, 0.3) 0px 10px,
          rgba(233, 66, 311, 0.2) 0px 15px,
          rgba(233, 66, 311, 0.1) 0px 20px,
          rgba(233, 66, 311, 0.05) 0px 25px;
      }
    `;
  }

  render() {
    return html`<nav>
      <slot></slot>
    </nav>`;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'sticky-nav': StickyNav;
  }
}
