import { LitElement, css, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';

@customElement('project-card')
export class ProjectCard extends LitElement {
  static get styles() {
    return css`
      :host {
      }

      .card {
        border-radius: 8px;
        box-shadow:
          rgba(0, 0, 0, 0.05) 0px 6px 24px 0px,
          rgba(0, 0, 0, 0.08) 0px 0px 0px 1px;
        position: relative;
      }

      .card img {
        object-fit: cover;
        width: 100%;
        height: 100%;
      }

      .card__content {
        width: calc(100% - 16px);
        padding: 8px;
        font-size: 0.8rem;
        background: rgba(0, 0, 0, 0.8);
        border-radius: 8px;
        color: white;
        position: absolute;
        bottom: 0;
        height: 48px;
        overflow: hidden;
        transition: height 0.5s ease;
      }

      .card:hover .card__content,
      .card__content:focus-within {
        height: calc(100% - 16px);
      }

      .card__content a:hover {
        opacity: 0.7;
      }

      .card__content__links a {
        text-decoration: none;
        border: 1px solid var(--pop-purple, inherit);
        border-radius: 4px;
        background: var(--pop-purple, inherit);
        color: white;
        padding: 2px 4px;
      }

      .card__content__links {
        margin: 8px 0;
      }
    `;
  }

  @property({ type: String, attribute: 'img-alt' }) imgAlt = 'image';
  @property({ type: String, attribute: 'img-src' }) imgSrc = '';
  @property({ type: String, attribute: 'code' }) codeHref = '';
  @property({ type: String, attribute: 'live' }) liveHref = '';

  render() {
    return html`
      <div class="card">
        <picture>
          <img src="${this.imgSrc}" alt="${this.imgAlt}" />
        </picture>
        <div class="card__content">
          <slot name="title"></slot>
          <div class="card__content__links">
            ${this.codeHref
              ? html`<a href="${this.codeHref}" target="_blank">code</a>`
              : ''}
            ${this.liveHref
              ? html`<a href="${this.liveHref}" target="_blank">live</a>`
              : ''}
          </div>
          <slot name="description"></slot>
        </div>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'project-card': ProjectCard;
  }
}
