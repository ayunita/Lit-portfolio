import { LitElement, css, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import './modal-dialog';
import '../data/highlights.json';
import { ModalDialog } from './modal-dialog';

type ModalHighlight = {
  title: string;
  subtitle: string;
  facts: string[];
};

@customElement('pop-highlight')
export class PopHighlight extends LitElement {
  static get styles() {
    return css`
      button {
        border: 1px solid var(--pop-purple, inherit);
        border-radius: 4px;
        background: var(--pop-purple, inherit);
        color: white;
        padding: 4px;
        cursor: pointer;
        font-family: "Fira Code", monospace;
      }

      button:hover {
        opacity: 0.7;
      }
    `;
  }

  private modal: ModalDialog | null = null;

  constructor() {
    super();
    this.modal = document.createElement('modal-dialog') as ModalDialog;
    document.body.appendChild(this.modal);
  }

  private _openModal(modalHighlight: ModalHighlight) {
    if (this.modal) {
      this.modal.open = true;
      this.modal.title = modalHighlight.title;
      this.modal.subtitle = modalHighlight.subtitle;
      this.modal.lines = modalHighlight.facts;
      this.modal.buttonLabel = 'Close';
      this.modal.buttonAction = () => {
        this._closeModal();
      };
      this.modal.backdropAction = () => {
        this._closeModal();
      };
    }
  }

  private _closeModal() {
    if (this.modal) {
      this.modal.dispatchEvent(new CustomEvent('button-click'));
      this.modal.open = false;
    }
  }

  @property({ type: String }) title = '';
  @property({ type: String }) subtitle = '';
  @property({
    reflect: true,
    converter: (attrValue: string | null) => {
      if (attrValue) {
        return attrValue.split(/,/g);
      } else {
        return undefined;
      }
    },
  })
  value: string[] = [];

  render() {
    return html`
      <button
        type="button"
        @click=${() =>
          this._openModal({
            title: this.title,
            subtitle: this.subtitle,
            facts: this.value,
          })}
      >
        <slot></slot>
      </button>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'pop-highlight': PopHighlight;
  }
}
