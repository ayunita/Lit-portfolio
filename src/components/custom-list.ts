import { LitElement, css, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';

@customElement('custom-list')
export class CustomList extends LitElement {
  static get styles() {
    return css`
      :host {
        --bullet-color: var(--pop-dark-gray, black);
      }

      ul {
        padding-left: 2rem;
      }

      li {
        counter-increment: li;
        display: list-item;
        line-height: 2;
        padding-inline-start: 0.5rem;
      }

      li::marker {
        color: var(--bullet-color);
        content: "#" counter(li) " ";
        font-size: 1.2rem;
        font-weight: 700;
      }
    `;
  }

  @property({ type: String }) description = 'description';

  render() {
    return html`
      <div>
        <p>${this.description}</p>
        <ul>
          <li><slot name="origin"></slot></li>
          <li><slot name="education"></slot></li>
          <li><slot name="work"></slot></li>
          <li><slot name="travel"></slot></li>
          <li><slot name="interest"></slot></li>
          <li><slot name="random"></slot></li>
        </ul>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'custom-list': CustomList;
  }
}
