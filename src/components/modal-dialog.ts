import { LitElement, TemplateResult, css, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { classMap } from 'lit/directives/class-map.js';

type ButtonActionType = () => void;
@customElement('modal-dialog')
export class ModalDialog extends LitElement {
  static get styles() {
    return css`
      :host {
        --bg-color: var(--pop-white, white);
        --divider-color: var(--pop-dark-gray, black);
        --button-color: var(--pop-dark-gray, black);
      }

      .backdrop {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.2);
        visibility: hidden;
      }

      .backdrop.open {
        visibility: visible;
      }

      .modal {
        background: var(--bg-color);
        position: fixed;
        width: 320px;
        height: 100%;
        top: 0;
        left: -320px;
        transform: translate3d(0%, 0, 0);
        -webkit-transform: translate3d(0%, 0, 0);
        transition:
          opacity 0.3s linear,
          left 0.3s ease-out;
        -webkit-transition:
          opacity 0.3s linear,
          left 0.3s ease-out;
        overflow-y: scroll;
      }

      .modal.open {
        left: 0;
      }

      .modal__container {
        padding: 2rem;
      }

      .modal__header {
        border-bottom: 1px solid var(--divider-color);
        padding-bottom: 1rem;
      }

      .modal__body {
        font-size: 0.9rem;
      }

      ol {
        list-style-position: inside;
        padding-left: 0;
      }

      ol li {
        margin-bottom: 1rem;
      }

      h2 {
        margin-top: 2rem;
        margin-bottom: 0;
      }

      button {
        border: 1px solid var(--button-color);
        color: var(--button-color);
        border-radius: 4px;
        background: transparent;
        padding: 4px;
        cursor: pointer;
      }

      button:hover {
        opacity: 0.5;
      }
    `;
  }

  @property({ type: Boolean }) open: boolean = false;
  @property({ type: String }) title: string = 'Title';
  @property({ type: String }) subtitle: string | TemplateResult<1> = 'Subtitle';
  @property({ type: Array }) lines: string[] = ['line 1', 'line 2', 'line 3'];
  @property({ type: String }) buttonLabel: string = 'Button';
  @property({ type: Function }) buttonAction: ButtonActionType = () => {};
  @property({ type: Function }) backdropAction: ButtonActionType = () => {};

  render() {
    return html` <div
        class="${classMap({ backdrop: true, open: this.open })}"
        @click=${this.backdropAction}
      ></div>
      <div
        class="${classMap({
          modal: true,
          open: this.open,
        })}"
        role="dialog"
        aria-modal="true"
      >
        <div class="modal__container">
          <div class="modal__header">
            <h2>${this.title}</h2>
            <span>${this.subtitle}</span>
          </div>
          <div class="modal__body">
            <ol>
              ${this.lines.map((line) => html` <li>${line}</li> `)}
            </ol>
          </div>
          <button @click=${this.buttonAction}>${this.buttonLabel}</button>
        </div>
      </div>`;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'modal-dialog': ModalDialog;
  }
}
