import { LitElement, css, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';

@customElement('icon-badge')
export class IconBadge extends LitElement {
  static get styles() {
    return css`
      :host {
      }

      .icon_badge {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }

      .icon_badge__icon ::slotted(*) {
        font-size: 2rem;
      }

      .icon_badge__text {
        font-family: "Fira Code", monospace;
        text-align: center;
        font-size: .8rem;
      }
    `;
  }

  @property({ type: String }) text = '';

  render() {
    return html`<div class="icon_badge">
      <div class="icon_badge__icon">
        <slot name="icon"></slot>
      </div>
      <div class="icon_badge__text">${this.text}</div>
    </div>`;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'icon-badge': IconBadge;
  }
}
