# Yunita's Portfolio

Website powered by Vite + Lit

# Installing

Get all dependencies
```
yarn
```

Run the app in dev mode
```
yarn dev
```